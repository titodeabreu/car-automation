package flow;

import static org.bytedeco.javacpp.lept.pixDestroy;
import static org.bytedeco.javacpp.lept.pixRead;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept.PIX;
import org.bytedeco.javacpp.tesseract.TessBaseAPI;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import bean.CarFileDTO;
import bean.StateDTO;

public class DownloadFileFlow {

	private final static String PATH_ARCHIVE = "C:\\car-automation\\";
	private static WebDriver driver = null;
	private static WebDriverWait wait = null;

	public static void main(String[] args) throws InterruptedException, IOException {
		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
		String arg = "";
		String stateToDownload = "";
		if(args != null && args.length > 0) {
			arg = args[0];
			stateToDownload = args[1];
		}
		boolean isContinue = false;
		List<String> filesDownloaded = new ArrayList<String>(0);
		isContinue = verifyDownloadProcess(arg, isContinue);
		filesDownloaded = updateFolder(isContinue, filesDownloaded);
		startAndSelectState();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("lista-estados-selecionados")));
		List<WebElement> states = driver.findElements(By.className("selecione-uf"));
		List<Map<String, CarFileDTO>> listMapDownloadControl = new ArrayList<>(0);
		Map<String, CarFileDTO> mapDownloadControl = null;
		CarFileDTO carFileDTO = null;
		List<StateDTO> stateNames = states.stream()
				.map(state -> new StateDTO(state.getText(), state.getAttribute("href"))).collect(Collectors.toList());

		updateDownloadList(isContinue, filesDownloaded, stateNames);
		System.out.println("Estado recebido para processamento: " + stateToDownload);
		for (StateDTO state : stateNames) {
			String normalized = Normalizer.normalize(state.getStateName(), Normalizer.Form.NFD);
			String stateNameWithoutAccent = normalized.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
			if (stateNameWithoutAccent.equals(stateToDownload)) {
				mapDownloadControl = new HashMap<String, CarFileDTO>();
				String stateName = state.getStateName();
				String stateDir = PATH_ARCHIVE + stateName;
				System.out.println("State Name:" + stateName);
				System.out.println("State dir:" + stateDir);
				driver.navigate().to(state.getStateHref());
	
				List<WebElement> counties = driver.findElements(By.className("item-municipio"));
				Integer countiesSize = counties.size();
				for (WebElement county : counties) {
					String countyName = county.getText();
						String countyDir = stateDir + "\\" + county.getText();
						System.out.println("State:" + countyName + " process.");
						carFileDTO = new CarFileDTO(stateName, countyName, stateDir, countyDir);
						WebElement button = county.findElement(By.className("btn-download"));
						button.click();
						System.out.println("State:" + countyName + " downloading.");
						downloadFile(mapDownloadControl, carFileDTO, button);
						Thread.sleep(1000l);
					
				}
				System.out.println("Size of Counties:" + countiesSize);
				listMapDownloadControl.add(mapDownloadControl);
				organizeDirectoryFiles(listMapDownloadControl);
				unzipAllFiles();
				closeDriver();
				startAndSelectState();
			}
		}
		System.out.println("Finished Download Car Files");
		closeDriver();
	}

	private static List<String> updateFolder(boolean isContinue, List<String> filesDownloaded) throws IOException {
		if (!isContinue) {
			cleanAndCreateDirectory();
		} else {
			filesDownloaded = Files.list(Paths.get(PATH_ARCHIVE)).filter(file -> Files.isDirectory(file))
					.map(file -> file.getFileName().toFile().toString()).collect(Collectors.toList());

			Files.list(Paths.get(PATH_ARCHIVE)).filter(file -> !Files.isDirectory(file)).forEach(file -> {
				try {
					Files.delete(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		}
		return filesDownloaded;
	}

	private static boolean verifyDownloadProcess(String arg, boolean isContinue) {
		if (arg != null && !arg.isEmpty()) {
			isContinue = Boolean.valueOf(arg);
		}
		return isContinue;
	}

	private static void updateDownloadList(boolean isContinue, List<String> filesDownloaded,
			List<StateDTO> stateNames) {
		if (isContinue && filesDownloaded != null && filesDownloaded.size() > 0) {
			for (String file : filesDownloaded) {
				stateNames.removeIf(state -> state.getStateName().equals(file));
			}
		}
	}

	private static void closeDriver() {
		driver.close();
	}

	private static void startAndSelectState() {
		startWebDriver();
		selectDowloadPage();
		WebElement selectState = driver.findElement(By.xpath("(//button[@type='button'])[2]"));
		selectState.click();
	}

	private static void unzipAllFiles() {
		Path p = Paths.get(PATH_ARCHIVE);
		FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				if (file.toAbsolutePath().toString().endsWith(".zip")) {
					unZipIt(file.toAbsolutePath().toString(), file.getParent().toString());
				}
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}
		};

		try {
			Files.walkFileTree(p, fv);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void organizeDirectoryFiles(List<Map<String, CarFileDTO>> listMapDownloadControl)
			throws IOException, InterruptedException {
		System.out.println("Start Organize Files");
		boolean isAllCountyFinished = true;
		do {
			if (Files.list(Paths.get(PATH_ARCHIVE)).anyMatch(path -> path.toString().contains(".part"))) {
				Thread.sleep(30000l);
			} else {
				isAllCountyFinished = false;
			}
		} while (isAllCountyFinished);
		listMapDownloadControl.forEach(countyMap -> {
			countyMap.keySet().forEach(pathString -> {
				try {
					CarFileDTO carFileDTO = countyMap.get(pathString);
					Path directory = Paths.get(carFileDTO.getCountyDir());
					FileUtils.moveFileToDirectory(carFileDTO.getPathFile().toFile(), directory.toFile(), true);
				} catch (IOException e) {
					System.err.println("Exception: " + e);
				}
			});
		});

	}

	private static void selectDowloadPage() {
		// SELECT DOWNLOAD PAGE
		driver.findElement(By.xpath("(//a[contains(text(),'BASE DE DOWNLOADS')])[2]")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@id='base-downloads']/div[2]/div/div[2]/div/div/button/h4/i")));
	}

	private static WebDriver setUpWebdriver() {
		System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		FirefoxOptions options = new FirefoxOptions();
		options.addPreference("browser.download.folderList", 2);
		options.addPreference("browser.download.dir", PATH_ARCHIVE);
		options.addPreference("browser.helperApps.neverAsk.saveToDisk", "application/zip");
		options.addPreference("pdfjs.disabled", true);
		WebDriver driver = new FirefoxDriver(options);
		return driver;
	}

	private static void startWebDriver() {
		// SET URL AND START
		driver = setUpWebdriver();
		wait = new WebDriverWait(driver, 20000);
		String baseUrl = "http://www.car.gov.br/publico/municipios/downloads";
		driver.get(baseUrl);
	}

	private static void cleanAndCreateDirectory() throws IOException {
		FileUtils.deleteDirectory(Paths.get(PATH_ARCHIVE).toFile());
		Files.createDirectory(Paths.get(PATH_ARCHIVE));
	}

	private static void downloadFile(Map<String, CarFileDTO> mapDownloadControl, CarFileDTO carFileDTO,
			WebElement countyDownloadButton) throws InterruptedException, IOException {
		String email = "edr@aeco.com";

		do {
			cleanDownloadBox();
			File imgElementByElement = retrieveCaptchaByElement();
			File imgElementCropped = retrieveCaptchaCropped();
			String captchaCropped = readCaptcha(imgElementCropped);
			;
			String captchaByElement = readCaptcha(imgElementByElement);
			if (captchaByElement != null && !captchaByElement.isEmpty()) {
				startDownload(email, captchaByElement);
			}
			if (captchaCropped != null && !captchaCropped.isEmpty()) {
				Thread.sleep(200);
				startDownload(email, captchaCropped);
			}
			if (captchaByElement == null || captchaByElement.isEmpty() && captchaCropped == null
					|| captchaCropped.isEmpty()) {
				closeDownloadModal();
				Thread.sleep(500);
				countyDownloadButton.click();
			}
			updateCaptcha();
		} while (isFileDownloading(mapDownloadControl, carFileDTO));
		closeDownloadModal();
	}

	private static void updateCaptcha() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("img-captcha-base-downloads")));
		driver.findElement(By.id("btn-atualizar-captcha")).click();
	}

	private static void closeDownloadModal() {
		driver.findElement(By.xpath("//div[@id='modal-download-base']/div/div/div/button/span")).click();
	}

	private static File retrieveCaptchaCropped() throws InterruptedException, IOException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("img-captcha-base-downloads")));
		// File imgElement =
		// driver.findElement(By.id("btn-baixar-dados")).getScreenshotAs(OutputType.FILE);
		WebElement ele = driver.findElement(By.id("img-captcha-base-downloads"));
		// Get entire page screenshot
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage fullImg = ImageIO.read(screenshot);

		// Get the location of element on the page
		Point point = ele.getLocation();

		// Get width and height of the element
		int eleWidth = ele.getSize().getWidth();
		int eleHeight = ele.getSize().getHeight();

		// Crop the entire page screenshot to get only element screenshot
		BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), 210, 140, 50);
		ImageIO.write(eleScreenshot, "png", screenshot);

		/*
		 * String logoSRC =
		 * driver.findElement(By.id("img-captcha-base-downloads")).getAttribute("src");
		 * 
		 * URL imageURL = new URL(logoSRC); File imgElement = new File("captcha.png");
		 * Files.delete(imgElement.toPath()); try(InputStream in = new
		 * URL(logoSRC).openStream()){ Files.copy(in, Paths.get("captcha.png")); }
		 * BufferedImage saveImage = ImageIO.read(imageURL); ImageIO.write(saveImage,
		 * "png", imgElement); Thread.sleep(200);
		 */
		return screenshot;
	}

	private static File retrieveCaptchaByElement() throws InterruptedException, IOException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("img-captcha-base-downloads")));
		return driver.findElement(By.id("img-captcha-base-downloads")).getScreenshotAs(OutputType.FILE);
	}

	private static boolean isFileDownloading(Map<String, CarFileDTO> mapDownloadControl, CarFileDTO carFileDTO)
			throws IOException {
		boolean fileStartedDownloading = false;
		List<Path> zipFiles = Files.list(Paths.get(PATH_ARCHIVE)).filter(path -> path.toString().endsWith(".zip"))
				.collect(Collectors.toList());

		for (Path path : zipFiles) {
			if (mapDownloadControl.get(path.toString()) == null) {
				carFileDTO.setPathFile(path);
				mapDownloadControl.put(path.toString(), carFileDTO);
				fileStartedDownloading = true;
			}
		}
		return fileStartedDownloading ? false : true;
	}

	private static void startDownload(String email, String captcha) throws InterruptedException {
		cleanDownloadBox();
		driver.findElement(By.id("form-email-download-base")).sendKeys(email);
		driver.findElement(By.id("form-captcha-download-base")).sendKeys(captcha);
		driver.findElement(By.xpath("//button[@id='btn-baixar-dados']/span")).click();
	}

	private static void cleanDownloadBox() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("form-email-download-base")));
		driver.findElement(By.id("form-email-download-base")).clear();
		driver.findElement(By.id("form-captcha-download-base")).clear();
	}

	@SuppressWarnings("resource")
	public static String readCaptcha(File captcha) {
		BytePointer outText;

		TessBaseAPI api = new TessBaseAPI();
		if (api.Init("tessdata", "ENG") != 0) {
			System.err.println("Could not initialize tesseract.");
			System.exit(1);
		}

		PIX image = pixRead(captcha.getAbsolutePath());
		api.SetImage(image);
		outText = api.GetUTF8Text();
		String captchValue = outText.getString();
		api.End();
		api.close();
		outText.deallocate();
		outText.close();
		pixDestroy(image);

		return captchValue;
	}

	public static void unZipIt(String zipFile, String outputFolder) {

		byte[] buffer = new byte[1024];

		try {

			// create output directory is not exists
			File folder = new File(outputFolder);
			if (!folder.exists()) {
				folder.mkdir();
			}

			// get the zip file content
			ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
			// get the zipped file list entry
			ZipEntry ze = zis.getNextEntry();

			while (ze != null) {

				String fileName = ze.getName();
				File newFile = new File(outputFolder + File.separator + fileName);

				System.out.println("file unzip : " + newFile.getAbsoluteFile());

				// create all non exists folders
				// else you will hit FileNotFoundException for compressed folder
				new File(newFile.getParent()).mkdirs();

				FileOutputStream fos = new FileOutputStream(newFile);

				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				fos.close();
				ze = zis.getNextEntry();
			}

			zis.closeEntry();
			zis.close();

			System.out.println("Done");

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

}
