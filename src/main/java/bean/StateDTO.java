package bean;

public class StateDTO {

	private String stateName;
	private String stateHref;

	public StateDTO(String stateName, String stateHref) {
		super();
		this.stateName = stateName;
		this.stateHref = stateHref;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateHref() {
		return stateHref;
	}

	public void setStateHref(String stateHref) {
		this.stateHref = stateHref;
	}

}
