package bean;

import java.nio.file.Path;

public class CarFileDTO {

	private String state;
	private String county;
	private String countyDir;
	private String stateDir;
	private Path pathFile;

	
	
	public CarFileDTO(String state, String county, String stateDir, String countyDir) {
		super();
		this.state = state;
		this.county = county;
		this.stateDir = stateDir;
		this.countyDir = countyDir;
	}

	
	public String getCountyDir() {
		return countyDir;
	}


	public void setCountyDir(String countyDir) {
		this.countyDir = countyDir;
	}


	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getStateDir() {
		return stateDir;
	}

	public void setStateDir(String stateDir) {
		this.stateDir = stateDir;
	}

	public Path getPathFile() {
		return pathFile;
	}

	public void setPathFile(Path pathFile) {
		this.pathFile = pathFile;
	}

}
